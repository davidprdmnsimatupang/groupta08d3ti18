<?php

namespace App\Models;

use CodeIgniter\Model;

class WisataModel extends Model
{
    protected $table = 'wisata';

    protected $useTimestamps = true;
    
    protected $allowedFields=['nama','gambar_url','kategori','deskripsi','latitude','longitude','gambar1','gambar2','gambar3','video','3d_file','create_at','update_at'];
    
    public function getWisata($nama = false)
    {
        if ($nama == false){
            return $this->findAll();
        }
        
        return $this->where(['nama'=>$nama])->first();
    }
    
     public function getWisataId($id = false)
    {
        if ($id == false){
            return $this->findAll();
        }
        
        return $this->where(['id'=>$id])->first();
    }
    
    
 

   
}