<div class="container">
    <div class="row">
        <div class="col">
             <center><h2 style="padding-top: 50px; padding-bottom:50px">Edit Informasi Wisata Toba</h2></center>
             <p align="right"><a href="/Pages/Update"  type="button" class="btn btn-primary">Kembali</a></p>
             <div class="alert alert-danger">
             <?= $validation->listErrors(); ?>    
             </div>
            <form action="/pages/perubahan/<?= $editwisata['id'];?>" method="post" enctype="multipart/form-data"> 
            <input type="hidden" name="gambarLama" value="<?= $editwisata['gambar_url'];?>">
            <input type="hidden" name="gambar1Lama" value="<?= $editwisata['gambar1'];?>">
            <input type="hidden" name="gambar2Lama" value="<?= $editwisata['gambar2'];?>">
            <input type="hidden" name="gambar3Lama" value="<?= $editwisata['gambar3'];?>">
            <input type="hidden" name="3dfileLama" value="<?= $editwisata['3d_file'];?>">
            
              <div class="row mb-3">
                <label for="nama" class="col-sm-2 col-form- label">Nama Wisata</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama" name="nama" 
                  value="<?= $editwisata['nama'];?>">
                </div>
              </div>
              
              <div class="row mb-3">
                <label for="gambar_url" class="col-sm-2 col-form-label">Gambar Android</label>
                <div class="col-sm-10">
                    <p><b>Gambar saat Ini</b> </p>
                    <img src="/Gambar_Android/<?= $editwisata['gambar_url'];?>" class="gambar ">
                    <p><?= $editwisata['gambar_url'];?></p>
                  <input type="file" class="form-control" id="gambar_url" name="gambar_url">
                </div>
              </div>
              
              <fieldset class="row mb-3">
                <legend class="col-form-label col-sm-2 pt-0">Kategori</legend>
                <div class="col-sm-10">
                    
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="kategori" id="gridRadios1" value="Air Terjun" <?= $editwisata['kategori']=='Air Terjun' ? ' checked' : '' ;?>>
                    <label class="form-check-label" for="gridRadios1" >
                    Air Terjun
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="kategori" id="gridRadios2" value="Bukit"<?= $editwisata['kategori']=='Bukit' ? ' checked' : '' ;?>>
                    <label class="form-check-label" for="gridRadios2">
                    Bukit
                    </label>
                  </div>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="kategori" id="gridRadios3" value="Pantai"<?= $editwisata['kategori']=='Pantai' ? ' checked' : '' ;?> >
                    <label class="form-check-label" for="gridRadios3">
                     Pantai
                    </label>
                  </div>
                </div>
              </fieldset>

              
              <div class="row mb-3">
                <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="<?= $editwisata['deskripsi'];?>">
                </div>
              </div>
              
              <div class="container-fluid" style="border: 1px;color: blue">
             <center>Lokasi Wisata</center>
              <div class="row mb-3">
                <label for="latitude" class="col-sm-2 col-form-label">Latitude</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="latitude" name="latitude" value="<?= $editwisata['latitude'];?>">
                </div>
              </div>
              
              <div class="row mb-3">
                <label for="longitude" class="col-sm-2 col-form-label">Longitude</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="longitude" name="longitude" value="<?= $editwisata['longitude'];?>">
                </div>
              </div>
              </div>
              
              <div class="container-fluid" style="border: 1px;color: red">
             <center>Gambar Pada Unity</center>
              <div class="row mb-3">
                <label for="gambar1" class="col-sm-2 col-form-label">Gambar 1</label>
                <div class="col-sm-10">
                     <p><b>Gambar saat Ini</b> </p>
                    <img src="/Gambar_Unity/<?= $editwisata['gambar1'];?>" class="gambar ">
                    <p><?= $editwisata['gambar1'];?></p>
                  <input type="file" class="form-control" id="gambar1" name="gambar1">
                </div>
              </div>

              <div class="row mb-3">
                <label for="gambar2" class="col-sm-2 col-form-label">Gambar 2</label>
                <div class="col-sm-10">
                    <p><b>Gambar saat Ini</b> </p>
                    <img src="/Gambar_Unity/<?= $editwisata['gambar2'];?>" class="gambar ">
                    <p><?= $editwisata['gambar2'];?></p>
                  <input type="file" class="form-control" id="gambar2" name="gambar2" >
                </div>
              </div>
            
            
              <div class="row mb-3">
                <label for="gambar3" class="col-sm-2 col-form-label">Gambar 3</label>
                <div class="col-sm-10">
                    <p><b>Gambar saat Ini</b> </p>
                    <img src="/Gambar_Unity/<?= $editwisata['gambar3'];?>" class="gambar ">
                    <p><?= $editwisata['gambar3'];?></p>
                  <input type="file" class="form-control" id="gambar3" name="gambar3" >
                </div>
              </div>
              </div>
              
              <div class="row mb-3">
                <label for="video" class="col-sm-2 col-form-label">Video</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="video" name="video" value="<?= $editwisata['video'];?>">
                </div>
              </div>
              
              
                <div class="row mb-3">
                <label for="3d_file" class="col-sm-2 col-form-label">3D File Unity</label>
                <div class="col-sm-10">
                    <p><b>Nama 3D File Saat Ini</b> </p>
                    <p><?= $editwisata['3d_file'];?></p>
                  <input type="file" class="form-control" id="3d_file" name="3d_file">
                </div>
              </div>
              
              <center><button type="submit" class="btn btn-warning">Perbaharui Wisata</button></center>
              <p>
            </form>
           
            
        </div>
    </div>
</div>