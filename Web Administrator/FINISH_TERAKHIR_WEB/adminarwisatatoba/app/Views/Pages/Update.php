<div class="container">
	<div class="row">
		<div class="col">
	   <center><h2 style="padding-top: 50px; padding-bottom: 50px">Update Informasi Wisata Toba</h2></center>
	   
	     <?php if(session()->getFlashdata('pesan'))  : ?>
	    
	   <div class="alert alert-warning" role="alert">
	    <?= session()->getFlashdata('pesan');?>
	    </div>
	    <?php endif; ?>
	       <table class="table table-success table-striped">
          <thead>
            <tr>
                <th scope="row">Id</th>
              <th class="col-md-3">Nama Wisata</th>
              <th class="col-md-3">Kategori</th>
              <th class="col-md-3">Gambar</th>
              <th class="col-md-3">Aksi</th>
            </tr>
          </thead>
          <tbody>
             
              <?php foreach($wisata as $a) :?>
            <tr>
               <th scope="row"><?= $a['id']; ?></th>
              <td><?= $a['nama']; ?></td>
              <td><?= $a['kategori']; ?></td>
              <td><img src="/Gambar_Android/<?= $a['gambar_url']?>" class="gambar" ></td>
              <td><a href="/pages/edit/<?= $a['id'];?>" type="button" class="btn btn-warning">Edit</a> </td>
            </tr>
            
         <?php endforeach ?> 
          </tbody>
        </table>
		</div>
	</div>
</div> 