<div class="container">
    <div class="row">
        <div class="col">
             <center><h2 style="padding-top: 50px; padding-bottom:50px">Tambah Informasi Wisata Toba</h2></center>
             <p align="right"><a href="/Pages/Tambah"  type="button" class="btn btn-primary">Kembali</a></p>
             
            <div class="alert alert-danger">
            
             <?= $validation->listErrors(); ?>    
             </div>
             
             
            <form action="/pages/save" method="post" enctype="multipart/form-data"> 
           
              <div class="row mb-3">
                <label for="nama" class="col-sm-2 col-form-label" >Nama Wisata</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama" name="nama"  autofocus value="<?= old('nama');?>" >
                </div>
              </div>
              
              <div class="row mb-2">
                <label for="gambar_url" class="col-sm-2 col-form-label">Gambar Android</label>
                <div class="col-sm-10">
                  <div class="input-group mb-3">
                  <input type="file" class="form-control" name ="gambar_url" id="gambar_url"  for="gambar_url" >
                </div>
                </div>
              </div>
              
              <fieldset class="row mb-3">
                <legend class="col-form-label col-sm-2 pt-0">Kategori</legend>
                <div class="col-sm-10">
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="kategori" id="gridRadios1" value="Air Terjun"  checked>
                    <label class="form-check-label" for="gridRadios1">
                     Air Terjun
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="kategori" id="gridRadios2" value="Bukit">
                    <label class="form-check-label" for="gridRadios2">
                    Bukit
                    </label>
                  </div>
                  <div class="form-check disabled">
                    <input class="form-check-input" type="radio" name="kategori" id="gridRadios3" value="Pantai" >
                    <label class="form-check-label" for="gridRadios3">
                     Pantai
                    </label>
                  </div>
                </div>
              </fieldset>
              
              <div class="row mb-3">
                <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="<?= old('deskripsi'); ?>" required>
                </div>
              </div>
              
              <div class="container-fluid" style="border: 1px;color: blue">
             <center>Lokasi Wisata</center>
              <div class="row mb-3">
                <label for="latitude" class="col-sm-2 col-form-label">Latitude</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="latitude" name="latitude" 
                  value="<?= old('latitude'); ?>"> 
                </div>
              </div>
              
              <div class="row mb-3">
                <label for="longitude" class="col-sm-2 col-form-label">Longitude</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="longitude" name="longitude" value="<?= old('longitude'); ?>">
                </div>
              </div>
              </div>
              
              <div class="container-fluid" style="border: 1px;color: red">
             <center>Gambar Pada Unity</center>
              <div class="row mb-3">
                <label for="gambar1" class="col-sm-2 col-form-label">Gambar 1</label>
                <div class="col-sm-10">
                <div class="input-group mb-3">
                  <input type="file" class="form-control" name ="gambar1" id="gambar1"  for="gambar1" >
                </div>
                </div>
              </div>

              <div class="row mb-3">
                <label for="gambar2" class="col-sm-2 col-form-label">Gambar 2</label>
                <div class="col-sm-10">
                  <div class="input-group mb-3">
                  <input type="file" class="form-control" name ="gambar2" id="gambar2"  for="gambar2" >
                </div>
                </div>
              </div>
            
            
              <div class="row mb-3">
                <label for="gambar3" class="col-sm-2 col-form-label">Gambar 3</label>
                <div class="col-sm-10">
                   <div class="input-group mb-3">
                  <input type="file" class="form-control" name ="gambar3" id="gambar3"  for="gambar3" >
                </div>
                </div>
              </div>
              </div>
              
              <div class="row mb-3">
                <label for="video" class="col-sm-2 col-form-label">Link Video</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="video" name="video" required value="<?= old('video');?>">
                </div>
              </div>
              
              
                <div class="row mb-3">
                <label for="3d_file" class="col-sm-2 col-form-label">3D File Unity</label>
                <div class="col-sm-10">
                 <div class="input-group mb-3">
                  <input type="file" class="form-control" name ="3d_file" id="3d_file"  for="3d_file" >
                </div>
                </div>
              </div>
              
              <center><button type="submit" class="btn btn-primary">Tambahkan Wisata</button></center>
              <p>
            </form>
           
            
        </div>
    </div>
</div>