<div class="container">
    <div class="row">
        <div class="col">
             <center><h2 style="padding-top: 50px; padding-bottom:50px">Detail Informasi Wisata Toba</h2></center>
             <p align="right"><a href="/Pages/Informasi"  type="button" class="btn btn-primary">Kembali</a></p>
            <table class="table table-success table-striped" border="2px" >
               
               
                 <tr>
                  <thead>     
                  <th class="col-md-3">Id Wisata</th>
                  <td> : </td>
                  <td><?= $detailwisata['id']; ?></td>
                  </thead>
                </tr>
                
                <tr>
                  <thead>     
                  <th class="col-md-3">Nama Wisata</th>
                  <td> : </td>
                  <td><?= $detailwisata['nama']; ?></td>
                  </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th class="col-md-3">Kategori</th>
                    <td> : </td>
                    <td><?= $detailwisata['kategori']; ?></td>
                    
                    </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th  class="col-md-3">Gambar Pada Android</th>
                    <td style="vertical-align: middle;"> : </td>
                    <td><img src="/Gambar_Android/<?= $detailwisata['gambar_url']?>" class="gambar" ></td>
                    </thead>
                </tr>
                
                <tr>
                  <thead>     
                  <th class="col-md-3" style="vertical-align: middle;">Informasi</th>
                  <td style="vertical-align: middle;"> : </td>
                  <td><?= $detailwisata['deskripsi']; ?></td>
                  </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th class="col-md-3">Latitude</th>
                    <td> : </td>
                    <td><?= $detailwisata['latitude']; ?></td>
                    </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th  class="col-md-3">Longitude</th>
                    <td> : </td>
                    <td><?= $detailwisata['longitude']; ?></td>
                    </thead>
                </tr>
                
                <tr>
                  <thead>     
                  <th class="col-md-3">Gambar 1 Unity</th>
                  <td style="vertical-align: middle;"> : </td>
                  <td><img src="/Gambar_Unity/<?= $detailwisata['gambar1']?>" class="gambar" ></td>
                  </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th class="col-md-3">Gambar 2 Unity</th>
                    <td style="vertical-align: middle;"> : </td>
                     <td><img src="/Gambar_Unity/<?= $detailwisata['gambar2']?>" class="gambar" ></td>
                    </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th  class="col-md-3">Gambar 3 Unity</th>
                    <td style="vertical-align: middle;"> : </td>
                     <td><img src="/Gambar_Unity/<?= $detailwisata['gambar3']?>" class="gambar" ></td>
                    </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th  class="col-md-3">Video Unity</th>
                    <td> : </td>
                    <td><?= $detailwisata['video']; ?></td>
                    </thead>
                </tr>
                
                <tr>
                    <thead>
                    <th  class="col-md-3">3D File Unity</th>
                    <td> : </td>
                    <td><?= $detailwisata['3d_file']; ?></td>
                    </thead>
                </tr>
              
            </table>
         
           
            
        </div>
    </div>
</div>