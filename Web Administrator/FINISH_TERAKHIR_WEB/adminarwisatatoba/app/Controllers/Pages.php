<?php

namespace App\Controllers;
use App\Models\WisataModel;


class Pages extends BaseController
{
    protected $wisataModel;
    public function __construct()
    {
        $this->wisataModel = new WisataModel();
    }
    
	public function index()     
	{

	    echo view('Layout/header');
	    echo view('Pages/dashboard');
		echo view('Layout/footer');
	}
	
	public function informasi()
	{
        $data =[
            'wisata'=>$this->wisataModel->getWisata()
            ];
        
        echo view('Layout/header');
		echo view('Pages/Informasi',$data);
		echo view('Layout/footer');
       
	}
	
	public function tambah()
	{
	    
         $data =[
            'wisata'=>$this->wisataModel->getWisata()
            ];
            
        
	    echo view('Layout/header');
		echo view('Pages/Tambah',$data);
		echo view('Layout/footer');
	
	}
	
	public function update()
	{
	     $data =[
            'wisata'=>$this->wisataModel->getWisata()
            ];
	    echo view('Layout/header');
		echo view('Pages/Update',$data);
		echo view('Layout/footer');
	}
	
	public function hapus()
	{
	     $data =[
            'wisata'=>$this->wisataModel->getWisata()
            ];
	    echo view('Layout/header');
		echo view('Pages/Hapus',$data);
		echo view('Layout/footer');
	}
	
	
	public function detail($nama)
	{
	    //$wisata = $this->wisataModel->getWisata($nama);
	    $data =[    
	        'detailwisata' => $this-> wisataModel->getWisata($nama)
	        ];
	       // dd($data);
	       echo view('Layout/header');
	       echo view('Pages/detail',$data); 
	       echo view('Layout/footer');
	}
	
	public function formtambah()
	{
	  /*  session();*/
	    
	    //$wisata = $this->wisataModel->getWisata($nama);
	    $data =[    
	        'validation' => \Config\Services::validation()
	        ];
	       // dd($data);
	       echo view('Layout/header');
	       echo view('Pages/formtambah',$data); 
	       echo view('Layout/footer');
	}
 
    public function edit($id)
	{
	    //$wisata = $this->wisataModel->getWisata($nama);
	    $data =[    
	        'validation' => \Config\Services::validation(),
	        'editwisata' => $this-> wisataModel->getWisataId($id)
	        ];
	       // dd($data);
	       echo view('Layout/header');
	       echo view('Pages/edit',$data); 
	       echo view('Layout/footer');
	}
	
	  public function delete($id)
	{
	    //$wisata = $this->wisataModel->getWisata($nama);
            $this->wisataModel->delete($id);
            session()->setFlashdata('pesan','Data Berhasil Dihapus');
	       return redirect()->to('/Pages/Hapus');
	      
	       
	}
	
	public function save()
	{
	    if (!$this->validate([
	        'nama' => [ 
	            'rules'=>'required|is_unique[wisata.nama]',
	            'errors'=>[
	                'required'=>'Nama Harus Diisi.',
	                'is_unique'=>'Nama Harus Unik'
	                ]
	            ],
	            'latitude' => [ 
	            'rules'=>'required|numeric[wisata.latitude]',
	            'errors'=>[
	                'required'=>'Latitude Harus Diisi.',
	                'numeric'=>'Latitude Harus Numerik'
	                ]
	            ],
	            'longitude' => [ 
	            'rules'=>'required|numeric[wisata.longitude]',
	            'errors'=>[
	                'required'=>'Longitude Harus Diisi.',
	                'numeric'=>'Longitude Harus Numerik'
	                ]
	            ],
	            'gambar_url' => [ 
	            'rules'=>'uploaded[gambar_url]|is_image[gambar_url]',
	            'errors'=>[
	                'uploaded'=>'Pilih Gambar Andorid terlebih dahulu.',
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ],
	            'gambar1' => [ 
	            'rules'=>'uploaded[gambar1]|is_image[gambar1]',
	            'errors'=>[
	                'uploaded'=>'Pilih Gambar 1 terlebih dahulu.',
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ],
	            'gambar2' => [ 
	            'rules'=>'uploaded[gambar2]|is_image[gambar2]',
	            'errors'=>[
	                'uploaded'=>'Pilih Gambar 2 terlebih dahulu.',
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ],
	            'gambar3' => [ 
	            'rules'=>'uploaded[gambar3]|is_image[gambar3]',
	            'errors'=>[
	                'uploaded'=>'Pilih Gambar 3 terlebih dahulu.',
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ],
	            '3d_file' => [ 
	            'rules'=>'uploaded[3d_file]',
	            'errors'=>[
	                'uploaded'=>'Pilih 3D Wisata terlebih dahulu.',
	                
	                ]
	            ]
	            
	            
	        ]))
	        
	        
	        {
	           
	       
	            return redirect()->to('/Pages/formtambah')->withInput();
	        }
	        $gbrandroid = $this->request->getFile('gambar_url');
	        $gbrandroid->move('Gambar_Android');
	        $namaGambarAndroid = $gbrandroid->getName();
	        
	        
	        
	        $gbrunity1 = $this->request->getFile('gambar1');
	        $gbrunity1->move('Gambar_Unity');
    	    $namaGambarUnity1 = $gbrunity1->getName();
    	   
    	   
	        $gbrunity2 = $this->request->getFile('gambar2');
    	    $gbrunity2->move('Gambar_Unity');
    	    $namaGambarUnity2 = $gbrunity2->getName();
	        
	        $gbrunity3 = $this->request->getFile('gambar3');
	        $gbrunity3->move('Gambar_Unity');
    	    $namaGambarUnity3 = $gbrunity3->getName();
    	   
	        $wisata3d = $this->request->getFile('3d_file');
	        $wisata3d->move('Blender');
    	    $nama3d = $wisata3d->getName();
	        
	  
    	  
    	       
    	  
    	  
    	        
	   // dd($this->request->getVar());
	       $this->wisataModel->save([
	      'nama' => $this->request->getVar('nama'),
	      'gambar_url' =>$namaGambarAndroid,
	      'kategori' => $this->request->getVar('kategori'),
	      'deskripsi' => $this->request->getVar('deskripsi'),
	      'latitude' => $this->request->getVar('latitude'),
	      'longitude' => $this->request->getVar('longitude'),
	      'gambar1' => $namaGambarUnity1,
	      'gambar2' => $namaGambarUnity2,
	      'gambar3' => $namaGambarUnity3,
	      'video' => $this->request->getVar('video'),
	      '3d_file' => $nama3d
	      
	      ]);
	      
	      session()->setFlashdata('pesan','Data Berhasil Ditambahkan');
	      return redirect()->to('/Pages/Tambah');
	  
	}
	
	
	public function perubahan($id)
	{
	    $db = \Config\Database::connect();
	  
        $query = $db->query("SELECT nama FROM wisata where id =".$id);
        $results = $query->getResultArray();
        
        foreach ($results as $row)
        {
    
         $row['nama'];
 
        }
        

	    if($row['nama'] == $this->request->getVar('nama')){
	         $rule_nama ='min_length[5]|required';
	       /*  return $rule_nama;*/
	    }
	    
	    else{
	          $rule_nama='min_length[5]|is_unique[wisata.nama]';
	         
	        /*return $rule_nama;*/
	    }
	    
	    
	     if (!$this->validate([
	       'nama' => [ 
	            'rules'=>$rule_nama,
	            'errors'=>[
	                'min_length'=>'Nama minimal 5 karakter.',
	                'is_unique'=>'Nama Harus Unik'
	                ]
	            ],
	            'latitude' => [ 
	            'rules'=>'required|numeric[wisata.latitude]',
	            'errors'=>[
	                'required'=>'Latitude Harus Diisi.',
	                'numeric'=>'Latitude Harus Numerik'
	                ]
	            ],
	            'longitude' => [ 
	            'rules'=>'required|numeric[wisata.longitude]',
	            'errors'=>[
	                'required'=>'Longitude Harus Diisi.',
	                'numeric'=>'Longitude Harus Numerik'
	                ]
	            ],
	             'gambar_url' => [ 
	            'rules'=>'is_image[gambar_url]',
	            'errors'=>[
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ],
	            'gambar1' => [ 
	            'rules'=>'is_image[gambar1]',
	            'errors'=>[
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ],
	            'gambar2' => [ 
	            'rules'=>'is_image[gambar2]',
	            'errors'=>[
	              
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ],
	            'gambar3' => [ 
	            'rules'=>'is_image[gambar3]',
	            'errors'=>[
	                'is_image'=>'Yang anda pilih bukan gambar'
	                ]
	            ]
	          
	            
	            
	        ]))
	        
	        
	        {
	          /*  $validation = \Config\Services::validation();*/
	       /*
	            return redirect()->to('/Pages/edit/'.$this->request->getVar('nama'))->withInput()->with('validation',$validation);*/
	            return redirect()->to('/Pages/edit/'.$id)->withInput();
	        }
	        
	        $gbrandroid = $this->request->getFile('gambar_url');
	        if($gbrandroid->getError()==4){
	            $namaGambarAndroid= $this->request->getVar('gambarLama');
	       }else{
	            $gbrandroid->move('Gambar_Android');
	             $namaGambarAndroid = $gbrandroid->getName();
	        }

	        
	        $gbrunity1 = $this->request->getFile('gambar1');
	        if($gbrunity1->getError()==4){
	             $namaGambarUnity1 = $this->request->getVar('gambar1Lama');
	        }else{
    	        $gbrunity1->move('Gambar_Unity');
        	    $namaGambarUnity1 = $gbrunity1->getName();
	        }
    	   
	        $gbrunity2 = $this->request->getFile('gambar2');
	        if($gbrunity2->getError()==4){
	            $namaGambarUnity2 = $this->request->getVar('gambar2Lama');
	        }else{
        	    $gbrunity2->move('Gambar_Unity');
        	    $namaGambarUnity2 = $gbrunity2->getName();
	        }
	        $gbrunity3 = $this->request->getFile('gambar3');
	        if($gbrunity3->getError()==4){
	            $namaGambarUnity3 = $this->request->getVar('gambar3Lama');
	        }else{
	             $gbrunity3->move('Gambar_Unity');
    	         $namaGambarUnity3 = $gbrunity3->getName();
	        }
	       
    	   
	        $wisata3d = $this->request->getFile('3d_file');
	        if($wisata3d->getError()==4){
	            $nama3d = $this->request->getVar('3dfileLama');
	        }else{
	            $wisata3d->move('Blender');
    	        $nama3d = $wisata3d->getName();
	        }
	        
	        
	        
	        
	        $this->wisataModel->save([
	        'id' => $id,
	      'nama' => $this->request->getVar('nama'),
	      'gambar_url' =>$namaGambarAndroid,
	      'kategori' => $this->request->getVar('kategori'),
	      'deskripsi' => $this->request->getVar('deskripsi'),
	      'latitude' => $this->request->getVar('latitude'),
	      'longitude' => $this->request->getVar('longitude'),
	      'gambar1' => $namaGambarUnity1,
	      'gambar2' => $namaGambarUnity2,
	      'gambar3' => $namaGambarUnity3,
	      'video' => $this->request->getVar('video'),
	      '3d_file' => $nama3d
	      
	      ]);
	      
	      session()->setFlashdata('pesan','Data Berhasil Diperbaharui');
	      return redirect()->to('/Pages/Update');
	    
	}


}
