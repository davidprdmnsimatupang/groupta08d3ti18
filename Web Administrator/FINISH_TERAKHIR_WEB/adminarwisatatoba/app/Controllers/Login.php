<?php

namespace App\Controllers;
use App\Models\LoginModel;


class Login extends BaseController
{

    public function index()
    {
        return view ('Login/userform');
    }
    
    public function login_action(){
        $loginmodel = new LoginModel();
        
        $username = $this->request->getPost('username');
        $password = sha1($this->request->getPost('password'));
    
      
        $cek = $loginmodel->get_data($username, $password);
        
        
        if (($username == $cek['username'])  && ($password == $cek['password']))
        {
            $simpansession=[
                'login' => true
                ];
             
             
            session()->set('username',$cek['username']);
            session()->set($simpansession);
            
            return redirect()->to('/pages');
        }
        else{
            session()->setFlashdata('gagal','Username atau Password Salah');
             return redirect()->to('/login');
        }
    }
   
    public function logout()
    {
        session()->destroy();
        return redirect()->to('/login');
    }
    
    
}