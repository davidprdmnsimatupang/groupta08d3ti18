-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 09 Agu 2021 pada 09.27
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id16647077_tugasakhir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(2, 'development', '05134426bcd175050b08193c02f14ccdafcca088');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wisata`
--

CREATE TABLE `wisata` (
  `id` int(11) NOT NULL,
  `nama` char(100) NOT NULL,
  `gambar_url` varchar(200) NOT NULL,
  `kategori` char(100) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `gambar1` char(200) NOT NULL,
  `gambar2` char(200) NOT NULL,
  `gambar3` char(200) NOT NULL,
  `video` varchar(1500) NOT NULL,
  `3d_file` char(200) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `wisata`
--

INSERT INTO `wisata` (`id`, `nama`, `gambar_url`, `kategori`, `deskripsi`, `latitude`, `longitude`, `gambar1`, `gambar2`, `gambar3`, `video`, `3d_file`, `created_at`, `updated_at`) VALUES
(1, 'Air Terjun Siboruon', 'Air Terjun Siboruon 1_1.jpg', 'Air Terjun', 'Lokasi : Air Terjun Siboruan, Balige, Sumatera UtaraFasilitas : Warung makanan/minumanPerjalanan/Rute: Air Terjun Siboruon terletak di dekat kota Balige. Jaraknya sekitar 7 km. Dari Balige, Anda harus memilih kendaraan Anda dengan hati-hati. Jalannya kurang bagus. Pastikan Anda datang selama musim panas. Pasalnya, musim hujan bisa meningkatkan risiko longsor. Dibutuhkan sekitar 1-2 jam dari Balige ke Desa Siboruon. Meski perjalanan tidak nyaman ini, Anda akan menemukan lokasi yang indah untuk bersantai. Desa dan air terjunnya luar biasa.', 2.2896333959918276, 99.08389493949583, 'Air Terjun Siboruon 1.jpg', 'Air Terjun Siboruon 2.jpg', 'Medan-Air-Terjun-Siboruon3.png', 'https://r7---sn-2uuxa3vh-jb3ez.googlevideo.com/videoplayback?expire=1625179324&ei=XPDdYNmbGdKe1gLOtILYBw&ip=185.143.231.149&id=o-AByfVHiMYWreSM6XtjOFFoc82mH80fAeuHbwXfTMy-k8&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=jkouZln37Fcni8slT3UqTd4G&cnr=14&ratebypass=yes&dur=347.091&lmt=1619167327417120&fexp=24001373,24007246&c=WEB&txp=5311224&n=1lWHcJ27nKVPC7&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIgFgmq5WEt-a-7tWpvjcBqeoDCw7VlJbbMJHxJPytcy0ICIQDtbw1m1Frje-fNijbh0L8Pa9PAiycxn6C3JYa4bpL9oQ%3D%3D&redirect_counter=1&rm=sn-c0qly7z&req_id=ac326f5c8e36a3ee&cms_redirect=yes&ipbypass=yes&mh=Ed&mip=36.71.223.139&mm=31&mn=sn-2uuxa3vh-jb3ez&ms=au&mt=1625157416&mv=m&mvi=7&pl=23&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRgIhAJUyrM-ug-BheLCBP-2-3LdRkQ5bGDpRCH1_Dhk4cWw7AiEA3YRljNvm2KBNQFRTAl2-CGgP165Ie32KmjocqQ-NBPc%3D', 'airterjunsiboruan_1', NULL, '2021-07-01 11:42:45'),
(2, 'Air Terjun Siguragura', 'Air Terjun Siguragura 2.jpeg', 'Air Terjun', 'Lokasi : Kabupaten Toba Samosir, Sumatera Utara. Lokasi ini merupakan dataran tinggi di pinggiran Danau Toba.Fasilitas : Akomodasi, toilet, gazeboTentang : ketinggian 250 meterPerjalanan/Rute : Untuk sampai di kawasan wisata Air Terjun Sigura-Gura, pengunjung yang berada di Kota Medan perlu melakukan perjalanan menuju Kabupaten Toba Samosir. Jarak antara Kota Medan dan Kabupaten Toba Samosir sekitar 250 km.Waktu Buka : 24 jamTiket : Rp. 10.000 per pengunjung', 2.548571335067917, 99.30262103900574, '2a.jpg', '2b.jpg', '2c.jpeg', 'https://r4---sn-uxa3vhnxa-230d.googlevideo.com/videoplayback?expire=1625209142&ei=1mTeYM6sGaSF_9EPhMmbmAw&ip=37.19.197.242&id=o-AAqLr5gcQrc3W3F3njTKVdSqJ9Kt-aqUYOUI1GgTNZrc&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=4XbAMeLYD1gu5QsvZe_A-0MG&cnr=14&ratebypass=yes&dur=155.480&lmt=1608828615239676&fexp=24001373,24007246&c=WEB&txp=5535432&n=uMjwYu0ARsf6d-&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRgIhAN2XWt501j6Jgq9Yn_DBp5S-kn44YSGVRvF8HetZCRDQAiEAz3DHNwzZcgRi6Utpq7duN1PvJmdt4bu6ZN04_wwhGdI%3D&title=SIGURA%20%20GURA%20%20ASAHAN%20%20SUMATERA%20%20UTARA&redirect_counter=1&rm=sn-ab5e7e76&req_id=15c753524146a3ee&cms_redirect=yes&ipbypass=yes&mh=6i&mip=114.125.30.97&mm=31&mn=sn-uxa3vhnxa-230d&ms=au&mt=1625187408&mv=m&mvi=4&pl=23&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRgIhAPsLWkG-JG1IA4s38vMC7tYlr7B6MXqgtEMBp3Hbmf8wAiEAsIC3aDzJolRQq4QwvYl-CHnQYaTdrAWL5C8AhZcf0uQ%3D', 'airterjunsiguragura', NULL, '2021-07-01 20:02:01'),
(3, 'Air terjun Simanimbo', 'Air terjun Simanimbo 1.jpg', 'Air Terjun', 'Lokasi : Desa Simanimbo, Kecamatan Pintu Pohan Meranti, Kab. Toba, Sumatera UtaraPerjalanan/Rute: Untuk mengunjungi air terjun ini, kami harus mengambil jalan memutar dari belakang Bendungan Sigura-Gura. Karena lokasi air terjun ini berseberangan dengan lokasi pintu masuk Bendungan Sigura-Gura.', 2.5141134683691697, 99.26325455619484, '3a.JPG', '3b.jpg', '3c.jpg', 'https://r2---sn-a5meknzr.googlevideo.com/videoplayback?expire=1624959458&ei=gpXaYN2gDYeegQei5rzYCA&ip=179.61.159.51&id=o-AF8UfbRYBV54MbR-cy9iwF2I5OZTSd6g_bgSY3-x4Lxv&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=tki75-Oo8Diwm8jnD-UNgq8G&cnr=14&ratebypass=yes&dur=247.408&lmt=1608623656098136&fexp=24001373,24007246&c=WEB&txp=6211222&n=8OnhxS7m4A87Yj-130pGO&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRgIhAL2ekVBgTWFVshKiat0s5Bj3g-rRTW217QwBT0xu6gQ7AiEA9HHsYjj3t4eqAT_7ysQBQpESW2oGNvMUkhRKfOxq9f8%3D&rm=sn-hgnls7l,sn-npoey7l&req_id=abe0430945aca3ee&ipbypass=yes&cm2rm=sn-2uuxa3vh-jb3e7r,sn-npolr7l&redirect_counter=4&cms_redirect=yes&mh=ac&mip=110.138.91.138&mm=39&mn=sn-a5meknzr&ms=ltr&mt=1624937776&mv=m&mvi=2&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRAIgIXOQyntWm-aIWitsVaGcBWdf7VZmOe7QGSNUR6dr_NoCIAOT2Ke5_1l2SyRP34qsVfUBXG3jZvpBJdf5VT9CgnSY', 'airterjunsimanimbo', NULL, '2021-06-28 22:38:22'),
(4, 'Air terjun Situmurun', 'Air terjun Situmurun 1.jpg', 'Air Terjun', 'Lokasi: Jonggi Nihuta, Lumban Julu, Hatinggian, Lumban Julu, Kabupaten Toba, Sumatera Utara.Tentang : ketinggian 70 meter dengan 7 tingkatanPerjalanan/Rute  Perjalanan untuk menuju air terjun ini hanya dapat di tempuh dengan menggunakan kapal motor (boat). Ada dua cara untuk mencapai tempat ini. Pertama melalui kota Parapat dengan durasi perjalanan lebih kurang 2 jam dan melalui kota Balige.', 2.537273309572344, 99.00847230406656, '4a.jpg', '4b.jpg', '4c.png', 'https://r2---sn-npoe7nlz.googlevideo.com/videoplayback?expire=1624955827&ei=U4faYMmeAcWbgAewxII4&ip=191.101.79.38&id=o-AFguaGUKc9KrUbZaYQ5nHZW5Mvo1qiJl_OCyRx0vNh1y&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=l3vBpQvMZLJ1VWFFHh_blDoG&cnr=14&ratebypass=yes&dur=492.448&lmt=1471219285510175&fexp=9466586,24001373,24007246&beids=9466586&c=WEB&n=_YTw01BRrQE77HN1IvbxN&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRgIhAKpYPXmUe9lB3wHIV7mrhu_mL8Hh9ot0isS3PSFHtU27AiEAvy3rFTHKbhuotoVpKUa2Yp5CQ3WmsWuLN6Un_Ks-6MA%3D&rm=sn-hgned7l&req_id=408a9c36c6e9a3ee&cm2rm=sn-2uuxa3vh-jb3ez7l,sn-npozs7l&ipbypass=yes&redirect_counter=3&cms_redirect=yes&mh=sa&mip=110.138.91.138&mm=34&mn=sn-npoe7nlz&ms=ltu&mt=1624934189&mv=m&mvi=2&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRAIgHM0WTkbA7Wv_lyF804OVISFQTRSW1m528hgVBYIDIegCIHWI-zGXKk7PZfwiuzDXrZyij31rZPERNnFZ5A5VE2ES', 'airterjunsitumurun', NULL, '2021-06-28 21:38:17'),
(5, 'Air terjun Taman Eden', 'Air terjun Taman Eden 1.JPG', 'Air Terjun', 'Lokasi: Desa Lumban Rang Sionggang Utara, Kecamatan Lumbar Julu, Kabupaten Toba, Sumatera Utara.Tentang: Air Terjun Taman Eden ini berada dalam lokasi Taman Eden 100 sebuah wisata hutan alami. Memiliki beraneka jenis pohon dan buah-buahan. Di lokasi ini terdapat tiga buah air terjun, yaitu Air Terjun 2 tingkat (500 m dari posko), Air Terjun Gua Kelelawar (5 km dari posko), dan Air Terjun 7 tingkat (11 km dari posko).Perjalanan/Rute: Dari Parapat, jarak Taman Eden 100 hanya sekitar 17 kilometer ke arah Balige. Berjarak sekitar 190 km dari kota Medan dengan waktu tempuh sekitar 4 jam perjalanan berkendara. Dapat ditempu dengan menggunakan kendaraan pribadi atau umum dengan kondisi jalan cukup baik.', 2.59475666566402, 99.03741632551181, '5a.JPG', '5b.jpg', '5c.jpg', 'https://r3---sn-npoeene7.googlevideo.com/videoplayback?expire=1624955949&ei=zYfaYKChFMiJ8gPA_b24BA&ip=185.143.231.149&id=o-ANCiPPkwY5Rdg1ZGsiNZ3CcSdhqxY83YF4V5u7uD-xX2&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=S7VktsmTlmy2YD6casRhdiwG&ratebypass=yes&dur=501.829&lmt=1599122154626878&fexp=24001373,24007246&c=WEB&txp=6216222&n=dUtQRQh_ihPDEYP7KGjEf&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRAIgbkzwbSRFbou5mL4a39woHEB0RC9M2tAEY99avimiTNQCIGP9_mNQRV8hPeiTSvn9ZZYAqxBSJNYxYDwQzOPZq9N-&rm=sn-hgnek7l&req_id=cd7e70db68f6a3ee&cm2rm=sn-2uuxa3vh-jb3ez7s,sn-nposr7s&ipbypass=yes&redirect_counter=3&cms_redirect=yes&mh=8K&mip=110.138.91.138&mm=34&mn=sn-npoeene7&ms=ltu&mt=1624934189&mv=m&mvi=3&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRQIgC4z-0llkPjM8mkFrsOLaau7JhgjJsrrBwq43YfIMmU0CIQDjE23arMQvwRlhDvW6qkSYpr-uIGGGZDGfBvRezGDrng%3D%3D', 'airterjuntamaneden', NULL, '2021-06-28 21:39:33'),
(6, 'Bukit Pahoda', 'Bukit Pahoda 1.jpg', 'Bukit', 'Lokasi : Lumban Silintong, Bukit Tarabunga, Balige, Kab. Toba, Sumatera UtaraPerjalanan/Rute  Lokasi Bukit Pahoda tidak begitu jauh dari pusat Kota Balige hanya butuh sekitar 30 menit sampai ke lokasi ini, lalu berjalan santai melalui hamparan sawah juga tugu-tugu pekuburan warga setempat. Setibanya di Puncak Bukit Pahoda Anda akan disuguhi pemandangan Danau Toba yang sangat mempesona', 2.3477026354266757, 99.04103894824355, '6a.jpg', '6b.jpg', '6c.jpg', 'https://r1---sn-2uuxa3vh-jb3l.googlevideo.com/videoplayback?expire=1624956024&ei=GIjaYMDLApeH7gPIh4_ADQ&ip=179.61.145.44&id=o-AIGGOGm95iFnK1C1DKHrK7eu47DMS1au-Dk-FfFB5F6c&itag=18&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=788ZumXVH3QdumorVVFM6kQG&gir=yes&clen=22152199&ratebypass=yes&dur=439.623&lmt=1614872392223186&fexp=24001373,24007246&c=WEB&txp=6210222&n=IZGGxp-OZXfEl8TlH3XrM&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIhAIdHmsmoCQqYByajKfkn9gPdmU8yXicQ-iSM4RyAgreqAiARY2HfUwGPEHxxnVAkN6IR7NJwt4k0roghE90Lx7iqvw==&redirect_counter=1&rm=sn-hgnl67l&req_id=8a6e457490aea3ee&cms_redirect=yes&ipbypass=yes&mh=7N&mip=110.138.91.138&mm=31&mn=sn-2uuxa3vh-jb3l&ms=au&mt=1624933720&mv=m&mvi=1&pcm2cms=yes&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pcm2cms,pl&lsig=AG3C_xAwRQIgFPJKD6F3065SYj1tT0kWT2k8yTcpAsa7hTmbN1gqK94CIQD_YWOvW8vgj9X5MRUFDxxAgIrXCakKVUsVqjVnl36aIA%3D%3D', 'bukitpahodanew', NULL, '2021-06-28 23:34:14'),
(7, 'Bukit paropo', 'Bukit Paropo 2.jpg', 'Bukit', 'Lokasi : Kec. Silahisabungan, Dairi, Sumatera UtaraPerjalanan/Rute : Berjarak sekitar 4 jam dari medan menuju silalahi atau sekitar 3 jam lebih dari pematang siantar.', 2.8454082489546337, 98.52808299219105, '7a.jpg', '7b.jpg', '7c.jpg', 'https://r3---sn-2uuxa3vh-jb3ez.googlevideo.com/videoplayback?expire=1624956262&ei=BonaYNOSNM-yx_APr8SdmAU&ip=185.143.231.149&id=o-APgF08bWsbPCJY9-fs69lUZ9sf4HnvJAxOPAJhORChbq&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=ev-ZVqMQWpmQQxnD32yYvVQG&ratebypass=yes&dur=424.344&lmt=1583420226842004&fexp=24001373,24007246&c=WEB&txp=6216222&n=3jurhCGut2uCOsz8QbLEQ&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIhAJfiGsp28CGSfNJUp1FiVguQjIRda5Wyf0Wsm1dgfaOjAiB59x7-mQaPqA78KUyYKU-cRy18A2IGYHT0TkDuyIaL9A%3D%3D&redirect_counter=1&rm=sn-hgnl67s&req_id=43bce187c4e2a3ee&cms_redirect=yes&ipbypass=yes&mh=jc&mip=110.138.91.138&mm=31&mn=sn-2uuxa3vh-jb3ez&ms=au&mt=1624933720&mv=m&mvi=3&pcm2cms=yes&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pcm2cms,pl&lsig=AG3C_xAwRAIgcZjbhquzcnf1UL6eFYnzazQEjLVQzTG-cVnUvsl-xngCIDJkTcLx2bhKtY81W1_15IQ8Iv1mnzGiIxVZm1IoFZ3L', 'bukitporoponew', NULL, '2021-06-28 23:34:47'),
(8, 'Bukit Senyum', 'Bukit Senyum 1.jpg', 'Bukit', 'Lokasi : Desa Motung, Ajibata, Toba, Sumatera UtaraPerjalanan/Rute : Menempuh perjalanan dari Parapat, Bukit Senyum berjarak 12 kmTentang : Biaya kunjungan Rp. 5.000/orang. Camping = Rp. 10.000', 2.635954695324425, 98.92155622310293, '8a.jpg', '8b.jpg', '8c.jpeg', 'https://r5---sn-2uuxa3vh-jb3ez.googlevideo.com/videoplayback?expire=1624956352&ei=YInaYKHUBYq-1wKapaboBQ&ip=179.61.167.31&id=o-AHDAjIbrtDgRPzSQehM6VNpiIBR4WKkbPEivnAqMXgWm&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=SIBHmaOThjauOqaSjvR80g0G&ratebypass=yes&dur=158.360&lmt=1608194843398902&fexp=24001373,24007246&beids=9466587&c=WEB&txp=6216222&n=irm_adkZ9LMUUKFDi06gw&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIhAMZ2LR4iQcPb4cfKmk0ZCgunFhdcuqZnGVV87AB6P4GUAiBMMBXMpiArI0NOO2OxYGpYoPJsRRwk6mQDTHfvHYkW3g%3D%3D&redirect_counter=1&rm=sn-i5hez76&req_id=6a7ffb187e35a3ee&cms_redirect=yes&ipbypass=yes&mh=Yl&mip=110.138.91.138&mm=31&mn=sn-2uuxa3vh-jb3ez&ms=au&mt=1624933720&mv=m&mvi=5&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRAIgMMHr0ri_lK1KdNMlb4BNmNtb3jrKc0Z9iB3omVxfhqwCIE93dSuVrYCE40Q94lmigYx2WjTFCHsu-EqMAmDnCEY7', 'bukitsenyumnew', NULL, '2021-06-28 23:35:12'),
(9, 'Bukit Tarabunga', 'Bukit Tarabunga 2.jpg', 'Bukit', 'Lokasi : Bukit Tarabunga, Balige, Kab. Toba, Sumatera UtaraFasilitas : Akomodasi, KafePerjalanan/Rute : Untuk menuju Bukit Tarabunga, dari Kota Balige, wisatawan hanya perlu mengikuti rute ke Tarabunga. Butuh sekitar 10 menit. Bukit ini terletak 5 km dari Balige. Jika pengunjung datang dari Tampahan, perjalanan memakan waktu sekitar 3 menit. Ketika mencari akomodasi, Anda dapat menemukannya dengan mudah di Balige. Begitu Anda menjelajahi bukit, Anda dapat menikmati makan siang yang lezat di kafe terapung. Terletak dekat dengan Lumban Silintong.', 2.3445509380085845, 99.01727089667673, '9a.jpg', '9b.jpg', '9c.jpg', 'https://r2---sn-2uuxa3vh-jb3ez.googlevideo.com/videoplayback?expire=1624956399&ei=j4naYLKvH5jg7gPVhorIDQ&ip=179.61.162.120&id=o-AO_gP801H_p8K4QtvCW3s90Pl-kUSKjHriGzDUry3pxn&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=B5Czq50Yf2jhnUhmD2WEwWAG&cnr=14&ratebypass=yes&dur=347.138&lmt=1570197465555412&fexp=24001373,24007246&beids=9466585&c=WEB&txp=6206222&n=56M8E8tS0lTKZLiK_U8wN&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRAIgBcm99Sxu-AqKkGm5HEZOJglg-PLdp0n0QfdYs1xGf6YCIBfFHKy-zMV5YIqR1vHPbrvCTICc7ZiuLgBkzPoghDhO&redirect_counter=1&rm=sn-hgne67l&req_id=13668d0dc69ca3ee&cms_redirect=yes&ipbypass=yes&mh=1a&mip=110.138.91.138&mm=31&mn=sn-2uuxa3vh-jb3ez&ms=au&mt=1624933720&mv=m&mvi=2&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRQIgbWHIS8mOzWhjuuLOIYQsQ4j5Judjv7H7DPKJTzHhZt0CIQCYhmpqGPOH253Dt3snwAWUx98JAxFTuuhSjEBz0SAdVw%3D%3D', 'bukittarabunganew', NULL, '2021-06-28 23:35:47'),
(10, 'Pantai Bulbul', 'Pantai Bulbul 2.jpg', 'Pantai', 'Lokasi : Desa Lumban Bulbul, Balige, Kab. Toba, Sumatera UtaraFasilitas : Banana boat, perahu, kapal sewa, ayunan, papan loncat, tenda/pondok, tempat istirahat, kano, homestay, warung makanan/minuman.Waktu buka : 08.00 – 19.00 WIBPerjalanan/Rute: Lumban Bulbul terletak di Balige, Tobasa. Jarak sekitar 2 km dari Balige. Anda harus meluangkan beberapa menit perjalanan untuk mencapai lokasi. Dari Balige, Anda bisa mengambil kendaraan sewaan atau kendaraan umum. Ojek adalah ide bagus. Perjalanan akan terasa nyaman dan singkat. Jadi, ini tidak merepotkan. Untuk akomodasi, Anda bisa memilih hotel atau rumah lokal. Pilihan kedua memang lebih baik.BagusBagusBagusBagusSangat Indah dan Menarik, Almost before we knew it, we had left the ground.A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music, or a noble picture, or a passage from the grander poets. It always does one good.Wisata Alam / Mendaki, Jelajah dan Pegunungan', 2.348530760635729, 99.07323524851789, '10a.jpg', '10b.jpg', '10c.jpg', 'https://r6---sn-2uuxa3vh-jb3l.googlevideo.com/videoplayback?expire=1624956472&ei=2InaYJrYHY6bgQfx7ZuwBw&ip=185.143.231.149&id=o-ANDZE-dkxXU9-2s3HOxfW8hdIh1NqK3nZGe6YedTIoef&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=AVaA-7B9molr1t5dTUD-JkEG&ratebypass=yes&dur=481.581&lmt=1608987893823119&fexp=24001373,24007246&c=WEB&txp=6216222&n=zyR2_HkNIx0lULhqZRZvm&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRgIhAKIaSviEmUA5G0VX82HdrczfF-5FY240gfBO5y95AzImAiEAmSoM_JfXbyAUkN6ImBV--x_aE6cE_ibYdkqSXNWnffo%3D&redirect_counter=1&rm=sn-hgnek7l&req_id=967a674aa0e0a3ee&cms_redirect=yes&ipbypass=yes&mh=ND&mip=110.138.91.138&mm=31&mn=sn-2uuxa3vh-jb3l&ms=au&mt=1624933720&mv=m&mvi=6&pcm2cms=yes&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pcm2cms,pl&lsig=AG3C_xAwRQIgMTDjhvOy9SQjbaE62mdHhIqdos277Ena9QMAlfKjbSkCIQCvpH-Q_xqgoaIbvNVuBOX7bT0id9xjQsFya9F0LGQLTg%3D%3D', 'pantaibulbul', NULL, '2021-06-28 21:48:15'),
(11, 'Raja Ampat', '4b14394831ff419f33f10030ca22e07e.jpg', 'Pantai', 'Beada di Timur Indonesia', 2.8505731606098332, 98.52808299219105, '77b3a529d23937d5fb416919e0f620dd.jpg', '7a5f7950dadb9545b6db9e3bb727da46.jpg', '7f6756cf5a20eaa7db39b67b066e20ea.jpg', 'https://r4---sn-npoeenee.googlevideo.com/videoplayback?expire=1624955165&ei=vYTaYMGNEYzB-gbcvJSYDA&ip=181.214.209.198&id=o-AJmcjZ_7AYgtgiwJGm9ZO_wIgHDgGOSCYPInAp9JRMT4&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=JVRQrG9IA0kBHtLnpFjwoskG&ratebypass=yes&dur=557.186&lmt=1609762879961881&fexp=24001373,24007246&c=WEB&txp=6216222&n=sCSa7EhEZTcgZPy7fv5Z_&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIgDawiEqABnhtyUUhs0gfAhgiER0nX2rthi5Fo-j6YSvQCIQDSBkyT4AHcdRwQ_xaEsNxeQIEzSJqi5H103Rp4M26uDQ%3D%3D&redirect_counter=1&rm=sn-hgner7z&req_id=b10a97d0aac9a3ee&cms_redirect=yes&mh=wM&mip=110.138.91.138&mm=34&mn=sn-npoeenee&ms=ltu&mt=1624932753&mv=m&mvi=4&pl=20&lsparams=mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRAIgWW2ICh8olcjSEcVjDzjoNd4ABSZGbgPQyXT56aLFB0kCIBjKWy7I3ZcAWUjoBc_R1GiamzFIWwVrrrAfW76nw8PO', 'bulbul', '2021-06-29 01:37:48', '2021-06-29 01:38:48');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `wisata`
--
ALTER TABLE `wisata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `wisata`
--
ALTER TABLE `wisata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
